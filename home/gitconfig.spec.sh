#!/bin/bash

# very simple to include additional configs without the need to do funky injection
# also worth noting that it is possible to include configs based on conditionals includeif
BASE_DIR=$(dirname $0)
FILEPATH="${BASE_DIR}/gitconfig.spec"

git config --global include.path $FILEPATH
