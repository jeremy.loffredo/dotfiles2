" theming config

" set color palette
set t_Co=256

set background=dark

if (has("termguicolors"))
  	set termguicolors
	let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
	let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
endif

colorscheme dracula
"colorscheme vividchalk 

" syntax highlighting on
syntax enable

" highlight search terms
hi Search ctermbg=30 ctermfg=None

let g:webdevicons_enable_nerdtree = 1
