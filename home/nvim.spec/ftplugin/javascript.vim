inoremap <C-L> console.log()
let javascript_coc = ['coc-eslint', 'coc-angular']
call extend(g:coc_global_extensions, javascript_coc)
