" install vim-plug if not installed

if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin()
" fast code completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" auto completion of { ( [
Plug 'jiangmiao/auto-pairs'
" vividchalk theme
Plug 'tpope/vim-vividchalk', {'as': 'vividchalk'}
" Git blame
Plug 'tpope/vim-fugitive'
" nerdtree
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
" devicon support for nerdtree
Plug 'ryanoasis/vim-devicons'
" dark theme
Plug 'dracula/vim', {'as': 'dracula'}
" required for vim-snipmate
Plug 'MarcWeber/vim-addon-mw-utils'
" required for vim-snipmate
Plug 'tomtom/tlib_vim'
" snipit engine
Plug 'garbas/vim-snipmate'
" collection of snippets
Plug 'honza/vim-snippets'
" git gutter shows git diff in gutter
Plug 'airblade/vim-gitgutter'
" taglist plugin - nerdtree for functions
Plug 'yegappan/taglist'
call plug#end()

" set coc extensions base
let g:coc_global_extensions = ['coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-swagger', 'coc-yaml', 'coc-webview']
