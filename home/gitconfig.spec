[core]
	editor = vim
	pager = less
	preloadindex = true
[help]
	autocorrect = 1
[branch]
	autosetuprebase = remote
[push]
	default = simple
[diff]
	tool = vimdiff
[difftool]
	prompt = false
[alias]
# simple replacements
	br = branch
	ci = commit
	co = checkout
	cb = checkout -b
	new = checkout -b
	cp = cherry-pick -x
	dc = diff --cached
	fp = format-patch
	ls = log --oneline
	st = status
	up = pull
	ff = pull --ff-only
	au = add -u
	rl = reflog
	d = difftool
	bn = rev-parse --abbrev-ref HEAD
	pu = push
################################################################################
# update scripts                                                               #
################################################################################
# rebase update 
# git upr BRANCH
# pull latest of BRANCH and rebase current branch based on it
	upr = "!f() { \
		: checkout ; \
		CB=`git bn`; \
		git co $1; \
		git up; \
		git co $CB; \
		git rebase $1; \
	}; f"
# merge update
# git upm BRANCH
# pull latest of BRANCH and merge it into current branch
	upm = "!f() { \
		: checkout ; \
		CB=`git bn`; \
		git co $1; \
		git up; \
		git co $CB; \
		git merge --no-ff $1; \
	}; f"
################################################################################
# branch management scripts                                                    #
################################################################################
# checkout branch and pull latest
# git con BRANCH
	con = "!f() { \
		: checkout ; \
		git co $1; \
		git up; \
	}; f"
# push branch and set upstream
# git put
# git put REMOTE
	put = "!f() { \
		CB=`git bn`; \
		REMOTE=$1; \
		if [ -z \"$REMOTE\" ]; then \
			REMOTE=`git remote | head -1`; \
		fi; \
		git pu -u $REMOTE $CB; \
	}; f"
################################################################################
# reporting                                                                    #
################################################################################
# simple one-line branch list
# default is to show which one is current head and to use color
# include argument c to remove color
# include argument h to remove head
	brl = "!f() { \
		wantcolor=true; \
		wanthead=true; \
		for arg in \"$@\"; do \
			case $arg in \
				c) \
					wantcolor=false \
					;; \
				h) \
					wanthead=false \
					;; \
				*) \
					;; \
			esac; \
		done; \
		format=''; \
		if [ \"$wanthead\" != 'false' ]; then \
			format='%(HEAD) '; \
		fi; \
		if [ \"$wantcolor\" != 'false' ]; then \
			format=\"$format %(color:yellow)%(refname:short)%(color:reset) %(color:red)%(objectname:short)%(color:reset) (%(color:green)%(committerdate:relative)%(color:reset)) %(upstream:short)\"; \
		else \
			format=\"$format %(refname:short) %(objectname:short) (%(committerdate:relative)) %(upstream:short)\"; \
		fi; \
		git for-each-ref --sort=committerdate refs/heads/ --format=\"$format\"; \
	}; f"
# search for remote branch
# git be SEARCHSTRING
# git be REMOBE SEARCHSTRING
	be = "!f() { \
		REMOTE=$1; \
		SS=$2; \
		if [ -z \"$SS\" ]; then \
			SS=$REMOTE; \
			REMOTE=`git remote | head -1`; \
		fi; \
		git ls-remote --exit-code $REMOTE | grep -i $SS | head -1 | awk '{print $2}'; \
	}; f"
# pull remote branch
# git pr BRANCHNAME
# git pr REMOTE BRANCHNAME
	pr = "!f() { \
		REMOTE=$1; \
		SS=$2; \
		if [ -z \"$SS\" ]; then \
			SS=$REMOTE; \
			REMOTE=`git remote | head -1`; \
		fi; \
		REF=`git be $REMOTE $SS`; \
		if [ -z \"$REF\" ]; then \
			echo 'not found'; \
			exit 1; \
		fi; \
		BRANCH=`echo $REF | sed -e 's/^refs\\/heads\\///' | tr -d '\\n'`; \
		git config --add remote.$REMOTE.fetch +$REF:refs/remotes/$REMOTE/$BRANCH; \
		git fetch; \
		git con $BRANCH; \
	}; f"
