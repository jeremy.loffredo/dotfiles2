set -g default-terminal "screen-256color"

# set up vi style navigation
unbind l
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# open new windows with same pwd
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"
