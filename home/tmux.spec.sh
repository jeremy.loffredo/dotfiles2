#!/bin/bash

BASE_DIR=$(dirname $0)
FILEPATH="${BASE_DIR}/tmux.spec"

isSourced=$(grep -c "$FILEPATH" $HOME/.tmux.conf 2>/dev/null)

if [ "$isSourced" -eq "0" ]; then
	echo "not there"
	printf "\n%s" "source-file $FILEPATH" >> "${HOME}/.tmux.conf"
fi
