#!/bin/bash

# create symlinks for each file in the nvim config
# I could just link the directory, but maybe I will want specific
# configurations for certain systems - I think this allows for more
# flexability

NVIMDIR=$HOME/.config/nvim
# do not want to add symlinks if the directory is already a link
if [[ -L $NVIMDIR ]]; then
	mv ${NVIMDIR} ${NVIMDIR}.old
fi

if [ ! -d $NVIMDIR ]; then
	mkdir -p $NVIMDIR
fi

VIMRC=$HOME/.vimrc
VIMDIR=$HOME/.vim
if [[ -L $VIMRC ]]; then
	mv ${VIMRC} ${VIMRC}.old
fi

if [[ -e $VIMDIR ]]; then
	mv ${VIMDIR} ${VIMDIR}.old
fi

if [ ! -d $VIMDIR ]; then
	mkdir $VIMDIR
fi

BASE_DIR=$(dirname $0)

link_files () {
	local TargetDir=$1
	for src in $(find -H "$BASE_DIR/nvim.spec" -type f)
	do
		ConfigDir=$(dirname $src | sed -r "s,${BASE_DIR}/nvim.spec,,")
		ConfigName=$(basename "$src")
	
		if [[ -n "$ConfigDir" && ! -d "${TargetDir}${ConfigDir}" ]]; then
			mkdir "${TargetDir}${ConfigDir}"
		fi
	
		Target="${TargetDir}${ConfigDir}/${ConfigName}"
		if [ ! -e "$Target" ]; then
			ln -s $src $Target
		fi
	done
}

link_files $NVIMDIR
link_files $VIMDIR

InitFile=${BASE_DIR}/nvim.spec/init.vim
ln -s $InitFile $VIMRC
