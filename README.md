# Dotfiles

My dotfiles as they work best for me.

## Usage

These dotfiles can be installed by running `bin/init` which will make mostly
non-destructive changes for most of the configurations. These dotfiles act as a
supplement to the current configuration in most cases. This allows for system
specific configurations.
